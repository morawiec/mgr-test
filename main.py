import logging
import plotly

from enum import Enum
from scenarios import ScenarioRunner


class LogLevel(Enum):
    DEBUG = logging.DEBUG
    INFO = logging.INFO


USERS = 5000
LOGLEVEL = LogLevel.INFO.value


def run():
    plotly.tools.set_credentials_file(username='****', api_key='******')
    logging.basicConfig(filename='test.log', level=LOGLEVEL)

    ScenarioRunner.create_users(users_num=USERS)
    ScenarioRunner.create_users_and_get_all_users(users_num=USERS)
    ScenarioRunner.create_user_then_get_all_users(users_num=USERS)
    ScenarioRunner.create_user_then_get_all_users_process(users_num=USERS)
    ScenarioRunner.create_users_and_get_concrete_user(users_num=USERS)
    ScenarioRunner.create_user_then_get_concrete_user(users_num=USERS)
    ScenarioRunner.create_user_then_add_to_queue(users_num=USERS)
    ScenarioRunner.create_user_then_add_to_queue_2(users_num=USERS)
    ScenarioRunner.create_user_then_add_to_queue_then_get_queue_process(users_num=USERS)
    ScenarioRunner.create_user_then_add_to_queue_then_get_queue(users_num=USERS)
    ScenarioRunner.create_user_and_add_to_queue_and_get_queue(users_num=USERS)
    ScenarioRunner.create_user_then_add_to_queue_then_delete_user_process(users_num=USERS)
    ScenarioRunner.create_user_then_add_to_queue_then_delete_user(users_num=USERS)
    ScenarioRunner.create_user_and_add_to_queue_and_delete_user(users_num=USERS)


if __name__ == '__main__':
    run()
