import logging

from client import Client
from controllers import UserController
from factories import UserFactory
from utils import calculate_statistics


class ScenarioRunner:
    users = None
    client = None
    controller = None

    @staticmethod
    def run_scenario(func):
        def run(**kwargs):
            try:
                if "users_num" in kwargs:
                    ScenarioRunner.setUp(kwargs["users_num"])
                func(users_num=kwargs["users_num"])
            finally:
                ScenarioRunner.tearDown()

        return run

    @staticmethod
    @run_scenario.__func__
    def signup():
        ScenarioRunner.controller.signup()

    @staticmethod
    @run_scenario.__func__
    def create_users(**kwargs):
        logging.info(f'create_users')
        title = f'Create {kwargs["users_num"]} users'
        signup_times = ScenarioRunner.controller.signup()
        # note: In case od deleting
        ScenarioRunner.controller.signin()
        calculate_statistics(signup_times, title)

    @staticmethod
    @run_scenario.__func__
    def create_users_and_get_all_users(**kwargs):
        logging.info(f'create_users_and_get_all_users')
        title = f'Get {kwargs["users_num"]} users'
        ScenarioRunner.controller.signup()
        ScenarioRunner.controller.signin()
        get_users_times = ScenarioRunner.controller.get_users()
        calculate_statistics(get_users_times, title)

    @staticmethod
    @run_scenario.__func__
    def create_user_then_get_all_users(**kwargs):
        logging.info(f'create_user_then_get_all_users')
        title = f'Get all users sequentially'
        get_users_times = ScenarioRunner.controller.signUp_then_getUsers()
        calculate_statistics(get_users_times, title)

    @staticmethod
    @run_scenario.__func__
    def create_user_then_get_all_users_process(**kwargs):
        logging.info(f'create_user_then_get_all_users_process')
        title = f'Get all users sequentially'
        get_users_times = ScenarioRunner.controller.signUp_then_getUsers_process()
        calculate_statistics(get_users_times, title)

    @staticmethod
    @run_scenario.__func__
    def create_users_and_get_concrete_user(**kwargs):
        logging.info(f'create_users_and_get_concrete_user')
        title = f'Filter user among {kwargs["users_num"]} users'
        ScenarioRunner.controller.signup()
        ScenarioRunner.controller.signin()
        get_user_times = ScenarioRunner.controller.get_user()
        calculate_statistics(get_user_times, title)

    @staticmethod
    @run_scenario.__func__
    def create_user_then_get_concrete_user(**kwargs):
        logging.info(f'create_user_then_get_concrete_user')
        title = f'Filter user sequentailly'
        get_user_times = ScenarioRunner.controller.signUp_then_getUser()
        calculate_statistics(get_user_times, title)

    @staticmethod
    @run_scenario.__func__
    def create_user_then_add_to_queue(**kwargs):
        logging.info(f'create_user_then_add_to_queue')
        title = f'Add users Queue'
        ScenarioRunner.controller.signup()
        ScenarioRunner.controller.signin()
        add_to_queue_times = ScenarioRunner.controller.add_to_queue()
        calculate_statistics(add_to_queue_times, title)

    # note this scenario sum times in all process
    @staticmethod
    @run_scenario.__func__
    def create_user_then_add_to_queue_2(**kwargs):
        logging.info(f'create_user_then_add_to_queue_2')
        title = f'Add users Queue'
        signup_times = ScenarioRunner.controller.signup()
        signin_times = ScenarioRunner.controller.signin()
        add_to_queue_times = ScenarioRunner.controller.add_to_queue()
        total_time = \
            [sum([signin_times[i], signup_times[i], add_to_queue_times[i]]) for i in range(len(signup_times))]
        calculate_statistics(total_time, title)

    # note here we monitor whole process
    @staticmethod
    @run_scenario.__func__
    def create_user_then_add_to_queue_then_get_queue_process(**kwargs):
        logging.info(f'create_user_then_add_to_queue_then_get_queue_process')
        title = f'Get queue - whole process'
        total_time = ScenarioRunner.controller.signUp_then_add_to_queue_then_get_queue_process()
        calculate_statistics(total_time, title)

    @staticmethod
    @run_scenario.__func__
    def create_user_then_add_to_queue_then_get_queue(**kwargs):
        logging.info(f'create_user_then_add_to_queue_then_get_queue')
        title = f'Get queue'
        total_time = ScenarioRunner.controller.signUp_then_add_to_queue_then_get_queue()
        calculate_statistics(total_time, title)

    @staticmethod
    @run_scenario.__func__
    def create_user_and_add_to_queue_and_get_queue(**kwargs):
        logging.info(f'create_user_and_add_to_queue_and_get_queue')
        title = f'Get queue among  {kwargs["users_num"]} users'
        ScenarioRunner.controller.signup()
        ScenarioRunner.controller.signin()
        ScenarioRunner.controller.add_to_queue()
        get_queue_times = ScenarioRunner.controller.get_queue()
        calculate_statistics(get_queue_times, title)

    # note here we monitor whole process
    @staticmethod
    @run_scenario.__func__
    def create_user_then_add_to_queue_then_delete_user_process(**kwargs):
        logging.info(f'create_user_then_add_to_queue_then_delete_user_process')
        title = f'Delete user - whole process'
        total_time = ScenarioRunner.controller.signUp_then_add_to_queue_then_delete_from_queue_process()
        calculate_statistics(total_time, title)

    @staticmethod
    @run_scenario.__func__
    def create_user_then_add_to_queue_then_delete_user(**kwargs):
        logging.info(f'create_user_then_add_to_queue_then_delete_user')
        title = f'Delete user - whole process'
        delete_time = ScenarioRunner.controller.signUp_then_add_to_queue_then_delete_from_queue()
        calculate_statistics(delete_time, title)

    @staticmethod
    @run_scenario.__func__
    def create_user_and_add_to_queue_and_delete_user(**kwargs):
        logging.info(f'create_user_and_add_to_queue_and_delete_user')
        title = f'Delete user queue among  {kwargs["users_num"]} users'
        ScenarioRunner.controller.signup()
        ScenarioRunner.controller.signin()
        ScenarioRunner.controller.add_to_queue()
        get_queue_times = ScenarioRunner.controller.delete_user_then_add_temp_user()
        calculate_statistics(get_queue_times, title)

    @staticmethod
    def setUp(user_num):
        ScenarioRunner.users = UserFactory.create(users_num=user_num)
        ScenarioRunner.client = Client()
        ScenarioRunner.controller = UserController(ScenarioRunner.users, ScenarioRunner.client)

    @staticmethod
    def tearDown():
        ScenarioRunner.controller.delete_users()
        ScenarioRunner.users = None
        ScenarioRunner.client = None
        ScenarioRunner.controller = None
