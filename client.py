import requests

from constants import app_constants, api_constants
from user import User
from utils import generate_url


class Client:
    def __init__(self):
        self.host = app_constants["host"]
        self.port = app_constants["port"]

    def signup(self, user: User):
        url = generate_url(self.host, self.port, api_constants["signup"])
        json_object = {"email": user.email,
                       "firstName": user.first_name,
                       "lastName": user.last_name,
                       "password": user.password
                       }
        response = requests.post(url, json=json_object)
        return {"status_code": response.status_code, "content": response.content}

    def signin(self, user: User):
        url = generate_url(self.host, self.port, api_constants["signin"])
        json_object = {"usernameOrEmail": user.email,
                       "password": user.password
                       }
        response = requests.post(url, json=json_object)
        user.token = response.json()["accessToken"]
        return {"status_code": response.status_code, "content": response.content}

    def delete_user(self, user: User):
        url = generate_url(self.host, self.port, api_constants["delete_user"])
        headers = {'Authorization': 'Bearer {}'.format(user.token)}
        response = requests.delete(url, headers=headers)
        return {"status_code": response.status_code, "content": response.content}

    def get_users(self, user: User):
        url = generate_url(self.host, self.port, api_constants["get_users"])
        headers = {'Authorization': 'Bearer {}'.format(user.token)}
        response = requests.get(url, headers=headers)
        return {"status_code": response.status_code, "content": response.content}

    def get_user(self, user: User):
        url = generate_url(self.host, self.port, api_constants["get_user"]+user.email)
        headers = {'Authorization': 'Bearer {}'.format(user.token)}
        response = requests.get(url, headers=headers)
        return {"status_code": response.status_code, "content": response.content}

    def add_to_queue(self, user: User):
        url = generate_url(self.host, self.port, api_constants["add_to_queue"])
        json_object = {"queueName": app_constants["queue_name"]}
        headers = {'Authorization': 'Bearer {}'.format(user.token)}
        response = requests.put(url, json=json_object, headers=headers)
        return {"status_code": response.status_code, "content": response.content}

    def get_queues(self, user: User):
        url = generate_url(self.host, self.port, api_constants["get_queues"])
        headers = {'Authorization': 'Bearer {}'.format(user.token)}
        response = requests.get(url, headers=headers)
        return {"status_code": response.status_code, "content": response.content}

    def get_queue(self, user: User):
        url = generate_url(self.host, self.port, api_constants["get_queue"]+app_constants["queue_name"])
        headers = {'Authorization': 'Bearer {}'.format(user.token)}
        response = requests.get(url, headers=headers)
        return {"status_code": response.status_code, "content": response.content}

    def delete_from_queue(self, user: User):
        url = generate_url(self.host, self.port, api_constants["delete_from_queue"])
        headers = {'Authorization': 'Bearer {}'.format(user.token)}
        response = requests.delete(url, headers=headers)
        return {"status_code": response.status_code, "content": response.content}
