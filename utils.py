import logging
import numpy as np
import plotly.plotly as py
import plotly.graph_objs as go

from constants import users_constants
from plotly import tools
from statistics import mean
from sklearn import linear_model


def create_email(i):
    return str(users_constants["first_name"] + users_constants["last_name"] + str(i) + "@domain.com").lower()


def generate_url(host, port, path):
    return "http://{}:{}/{}".format(host, port, path)


def plot_subplots(data, titles):
    fig = tools.make_subplots(rows=len(data.keys()), cols=2, subplot_titles=titles)
    for num, (key, data_bunch) in enumerate(data.items()):
        trace = go.Scatter(
            x=[x for x in range(0, len(data_bunch))],
            y=data_bunch
        )
        fig.append_trace(trace, num + 1, 1)
        if key != "mean_trend":
            trace2 = go.Scatter(
                x=[x for x in range(0, len(data_bunch))],
                y=calculate_linear_regression(data_bunch)
            )
            fig.append_trace(trace2, num + 1, 2)
        fig['layout']['xaxis{}'.format(num+1)].update(title='User')
        fig['layout']['yaxis{}'.format(num+1)].update(title='Time [ms]')
    py.plot(fig, filename='basic-line', auto_open=True)


def calculate_mean_trend(data):
    return [mean(data[:i+1]) for i in range(len(data))]


def calculate_total_time(data):
    return sum(data)


def calculate_mean(data):
    return mean(data)


def calculate_linear_regression(data):
    regr = linear_model.LinearRegression()
    x = np.array([x for x in range(0, len(data))]).reshape(-1, 1)
    y = np.array(data).reshape(-1, 1)
    regr.fit(x, y)
    return list(regr.predict(x))


def calculate_statistics(recived_data, title):
    processed_data = remove_outliers(recived_data)
    outliers_num = abs(len(recived_data) - len(processed_data))
    mean_trend = calculate_mean_trend(processed_data)
    data = {title: processed_data, "mean_trend": mean_trend}
    titles = [title, "Linear regression", "Mean trend"]
    plot_subplots(data, titles)
    total_time = calculate_total_time(processed_data)
    mean = calculate_mean(processed_data)
    logging.info(f'Total time:  {total_time}, mean {mean}')
    logging.info(f'Found outliers {outliers_num}')


def remove_outliers(data):
    """ source  https://medium.com/datadriveninvestor/finding-outliers-in-dataset-using-python-efc3fce6ce32 """
    threshold = 3
    calculated_mean = mean(data)
    calculated_std = np.std(data)
    return [y for y in data if np.abs((y - calculated_mean) / calculated_std) <= threshold]
