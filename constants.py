users_constants = {
    "first_name": "Pawel",
    "last_name": "Nowak",
    "password": "password"
}

app_constants = {
    "host": "localhost",
    "port": "5000",
    "queue_name": "kolejka"
}

api_constants = {
    "signup": "api/auth/signup",
    "signin": "api/auth/signin",
    "delete_user": "api/users",
    "get_user": "api/users/",
    "get_users": "api/users",
    "add_to_queue": "api/queues",
    "delete_from_queue": "api/queues/user",
    "get_queue": "api/queues/",
    "get_queues": "api/queues",
}

