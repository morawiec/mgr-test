from constants import users_constants
from user import User
from utils import create_email


class UserFactory:

    @staticmethod
    def create(users_num):
        return [User(first_name=users_constants["first_name"] + str(i),
                     last_name=users_constants["last_name"] + str(i),
                     email=create_email(i),
                     password=users_constants["password"]) for i in range(users_num)]
