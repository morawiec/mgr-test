import logging
import time

from client import Client


class UserController:

    def __init__(self, users, client: Client):
        self.users = users
        self.client = client

    def delete_users(self):
        times = []
        for user in self.users:
            start_time = time.time()
            self.client.delete_user(user)
            times.append(time.time() - start_time)
        return times

    def delete_user_then_add_temp_user(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response1 = self.client.delete_user(user)
            times.append(time.time() - start_time)
            response2 = self.client.signup(user)
            response3 = self.client.add_to_queue(user)
            UserController.log(user.email, response1["status_code"], response1["content"])
            UserController.log(user.email, response2["status_code"], response2["content"])
            UserController.log(user.email, response3["status_code"], response3["content"])
        return times

    def get_users(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.get_users(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
        return times

    def get_user(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.get_user(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
        return times

    def add_to_queue(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.add_to_queue(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
        return times

    def get_queues(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.get_queues(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
        return times

    def get_queue(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.get_queue(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
        return times

    def delete_from_queue(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.delete_from_queue(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
        return times

    def signup(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.signup(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
        return times

    def signin(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.signin(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
        return times

    def signUp_then_getUser(self):
        times = []
        for user in self.users:
            response = self.client.signup(user)
            response2 = self.client.signin(user)
            start_time = time.time()
            response3 = self.client.get_user(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
            UserController.log(user.email, response2["status_code"], response2["content"])
            UserController.log(user.email, response3["status_code"], response3["content"])
        return times

    def signUp_then_getUsers(self):
        times = []
        for user in self.users:
            response = self.client.signup(user)
            response2 = self.client.signin(user)
            start_time = time.time()
            response3 = self.client.get_users(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
            UserController.log(user.email, response2["status_code"], response2["content"])
            UserController.log(user.email, response3["status_code"], response3["content"])
        return times

    def signUp_then_getUsers_process(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.signup(user)
            response2 = self.client.signin(user)
            response3 = self.client.get_users(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
            UserController.log(user.email, response2["status_code"], response2["content"])
            UserController.log(user.email, response3["status_code"], response3["content"])
        return times

    def signUp_then_add_to_queue_then_get_queue_process(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.signup(user)
            response2 = self.client.signin(user)
            response3 = self.client.add_to_queue(user)
            response4 = self.client.get_queue(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
            UserController.log(user.email, response2["status_code"], response2["content"])
            UserController.log(user.email, response3["status_code"], response3["content"])
            UserController.log(user.email, response["status_code"], response4["content"])
        return times

    def signUp_then_add_to_queue_then_get_queue(self):
        times = []
        for user in self.users:
            response = self.client.signup(user)
            response2 = self.client.signin(user)
            response3 = self.client.add_to_queue(user)
            start_time = time.time()
            response4 = self.client.get_queue(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
            UserController.log(user.email, response2["status_code"], response2["content"])
            UserController.log(user.email, response3["status_code"], response3["content"])
            UserController.log(user.email, response["status_code"], response4["content"])
        return times

    def signUp_then_add_to_queue_then_delete_from_queue_process(self):
        times = []
        for user in self.users:
            start_time = time.time()
            response = self.client.signup(user)
            response2 = self.client.signin(user)
            response3 = self.client.add_to_queue(user)
            response4 = self.client.delete_from_queue(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
            UserController.log(user.email, response2["status_code"], response2["content"])
            UserController.log(user.email, response3["status_code"], response3["content"])
            UserController.log(user.email, response["status_code"], response4["content"])
        return times

    def signUp_then_add_to_queue_then_delete_from_queue(self):
        times = []
        for user in self.users:
            response = self.client.signup(user)
            response2 = self.client.signin(user)
            response3 = self.client.add_to_queue(user)
            start_time = time.time()
            response4 = self.client.delete_from_queue(user)
            times.append(time.time() - start_time)
            UserController.log(user.email, response["status_code"], response["content"])
            UserController.log(user.email, response2["status_code"], response2["content"])
            UserController.log(user.email, response3["status_code"], response3["content"])
            UserController.log(user.email, response["status_code"], response4["content"])
        return times

    @staticmethod
    def log(email, status_code, content):
        if status_code != 200:
            logging.error(f'User:{email}, Status code {status_code}, Message {content}')
        else:
            logging.debug(f'User:{email}, Status code {status_code}, Message {content}')
